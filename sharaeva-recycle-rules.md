#Виртуальная лаборатория для проверки навыков сортировки вторсырья

##Сведения об учащемся
Шараева Кристина Витальевна, P42211

##Краткое описание
В качестве упражнения обучающемуся будут предложены картинки различного вторсырья, которое нужно будет распределить по нескольким категориям, например: 
- Стекло 
- Плаcтик pet 1
- Пластик pet 5
- Бумага 
- Картон 
- Металл
- Несортируемые отходы

Список категорий в дальнейшем может быть расширен.

##Аналоги 
В реальой жизни часто проводятся МК по сортировке отходов, на которых также можно проверить свои знания в правильности распределения вторсырья на фракции. Виртаульных аналогов найти не удалось. 

##Детали реализации: 
Предварительно для реализации был выбран следующий стек технологий:
Frontend: HTML, CSS, JavaScrip + TS
Backend: Node, RLCP
Система контроля версий: git 
Нотация для проектирования: UML
